#!/usr/bin/env python3
import argparse
import string
import sys
# local shared package
from karabiner_config_helpers import get_resolver, NoResolverWithName, CharRequiresMultipleKeys, NoKeyMappingFoundError, KeyResolver, helper_default_argument_parser_action_handler
from karabiner_config_helpers.app_layouts import load_app_resolver_map_json


def url_encode_character(character: str) -> str:
    character_to_two_digit_hex = hex(ord(character)).replace('x','0')[-2:]
    return f"%{character_to_two_digit_hex.lower()}"


def manipulator_for_character(character: str, resolver: KeyResolver, from_keys: dict, condition_list: list[dict]) -> dict:
    to_keys = []
    for char_to_type in url_encode_character(character):
        try:
            to_keys += resolver.get_to_keys_for_char(char_to_type)
        except NoKeyMappingFoundError:
            # Should not happen, the following characters should always be typeable: 0-9a-f%
            print(f"[!] Error: Can to type key '{char_to_type}'")
            exit(1)

    return {
        "type": "basic",
        "from": from_keys,
        "to": to_keys,
        "conditions": condition_list,
    }


def manipulators_for_most_characters(resolver: KeyResolver, description: str, extra_modifiers: list[str], condition_list: list[dict]) -> dict:
    ignored_chars = ["\r", "\x0b", "\x0c"]
    manipulator_list: list[dict] = []
    for character in string.printable:
        try:
            if character not in ignored_chars:
                from_keys = resolver.get_from_keys_for_char(character, extra_modifiers)
                manipulator_list.append(manipulator_for_character(character, resolver, from_keys, condition_list))
        except NoKeyMappingFoundError:
            print(f"Ignoring character {repr(character)} because no key mapping is found", file=sys.stderr)
        except CharRequiresMultipleKeys:
            print(f"Ignoring character {repr(character)} because it maps to multiple keystrokes", file=sys.stderr)

    for carriage_return_key in ["left_arrow", "home"]:
        from_keys = {
            "key_code": carriage_return_key,
            "modifiers": {
                "mandatory": extra_modifiers,
                "optional": [
                    "caps_lock"
                ],
            },
        }
        manipulator_list.append(manipulator_for_character("\r", resolver, from_keys, condition_list))

    return manipulator_list


def generate_config_json(args) -> dict:
    if args.description != None:
        description = args.description
    else:
        human_keycombo = "+".join(sorted(args.keybinding_modifiers))
        description = f"-60 | #URL_ENCODE_{human_keycombo.upper()} | {human_keycombo}+<KEY> -> Type URL encoded versions of the characters you type (space -> %20)"

    app_resolver_map = load_app_resolver_map_json()
    manipulators: list[dict] = []
    for resolver_name, condition_list in app_resolver_map.get_resolver_names_and_conditions():
        key_resolver = get_resolver(resolver_name)
        manipulators += manipulators_for_most_characters(key_resolver, description, args.keybinding_modifiers, condition_list)

    return {
        "description": description,
        "manipulators": manipulators,
    }


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("keybinding_modifiers", nargs="*", default=["fn"], help="what modifiers need to be pressed for the keybinding (example: 'right_command' 'right_meta', default: 'fn')")
    ap.add_argument("-d", "--description", help="use this value as a description instead of automatically generating one")

    helper_default_argument_parser_action_handler(ap, generate_config_json)
