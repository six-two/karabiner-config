#!/usr/bin/env python3
import argparse
import os
import shlex
# local shared package
from karabiner_config_helpers import helper_default_argument_parser_action_handler

DEFAULT_FROM_BUTTON_COPY = "button5"
DEFAULT_FROM_BUTTON_PASTE = "button4"
DEFAULT_FROM_KEY_COPY = "c"
DEFAULT_FROM_KEY_PASTE = "v"
DEFAULT_FROM_MODIFIER = "right_option"
DEFAULT_HOLD_MILLIS = 250


def generate_config_json(args) -> dict:
    copy_button = DEFAULT_FROM_BUTTON_COPY # @TODO: make cmdline args?
    paste_button = DEFAULT_FROM_BUTTON_PASTE
    copy_key = args.copy_key
    paste_key = args.paste_key
    to_clipboard_hold_millis = args.hold_millis
    modifier = args.from_modifier

    SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

    # Make sure that this script is in the correct location
    script_path = os.path.join(SCRIPT_DIR, "type-clipboard-update-script.sh")
    if not os.path.exists(script_path):
        print("[!] File does not exist:", script_path)
        exit(1)

    clipboard_to_karabiner_action = {
        "shell_command": f"{shlex.quote(script_path)} '-2 | #CB_TYPE_V | right_option+v / button4: Type clipboard' --sleep {args.sleep} {modifier}+{paste_key} {paste_button}"
    }

    key_from_json = {
        "key_code": copy_key,
        "modifiers": {
            "mandatory": [
                modifier,
            ],
            "optional": [
                "caps_lock",
            ],
        },
    }

    mouse_from_json = {
        "pointing_button": copy_button,
        "modifiers": {
            "mandatory": [],
            "optional": [
                "caps_lock",
            ],
        },
    }

    manipulators = []
    for from_input_event_json in [key_from_json, mouse_from_json]:
        manipulators.append({
            "type": "basic",
            "from": from_input_event_json,
            "parameters": {
                "basic.to_if_alone_timeout_milliseconds": to_clipboard_hold_millis,
                "basic.to_if_held_down_threshold_milliseconds": to_clipboard_hold_millis
            },
            "to_if_alone": [
                # Short type: write current clipboard to config
                clipboard_to_karabiner_action
            ],
            "to_if_held_down": [
                # Hold button down
                # Copy current selection by sending Command-C write the new clipboard to config
                #@TODO check correct keyboard combi for OS
                {
                    "key_code": "c",
                    "modifiers": [
                        "left_command",
                    ],
                },
                # Wait a little
                {
                    "key_code": "vk_none",
                    "hold_down_milliseconds": 150,
                },
                # Write current clipboard to config
                clipboard_to_karabiner_action,
            ],
        })

    full_json = {
        "description": f"-1 | #CB_UPDATE | {modifier}+{copy_key} / {copy_button} -> Store clipboard contents for later typing",
        "manipulators": manipulators,
    }
    return full_json


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--copy-key", default=DEFAULT_FROM_KEY_COPY, help=f"the key to use for triggering the copy action (store clipboard). Default: {DEFAULT_FROM_KEY_COPY}")
    ap.add_argument("--paste-key", default=DEFAULT_FROM_KEY_PASTE, help=f"the key to use for triggering the paste action (type stored clipboard). Default: {DEFAULT_FROM_KEY_PASTE}")
    ap.add_argument("-m", "--from-modifier", default=DEFAULT_FROM_MODIFIER, help=f"the modifier key to use for triggering the shortcut. May not contain shift, because it is needed for the read from clipboard and type clipboard differentiation. Default: {DEFAULT_FROM_MODIFIER}")
    ap.add_argument("-d", "--hold-millis", type=int, default=DEFAULT_HOLD_MILLIS, help=f"after how many milli seconds of wolding down the mouse button to first copy the currently selected text. Default: {DEFAULT_HOLD_MILLIS}")
    ap.add_argument("-s", "--sleep", type=float, default=1, help="how long to wait before starting the typing")

    # Insert the rule or print it depending on whether --print is supplied
    helper_default_argument_parser_action_handler(ap, generate_config_json)
