#!/usr/bin/env python3
# calls copy-qr-code from gitlab.com/six-two/bin
import argparse
import shlex
import shutil
# local shared package
from karabiner_config_helpers import helper_default_argument_parser_action_handler



def generate_config_json(args) -> dict:
    # I had problems with it not being in the path due to non-interactive shells and MacOS's login process. So we get the full path.
    copy_qr_code_path = shutil.which("copy-qr-code")
    # This script loads the correct venv, that is needed because of library dependencies. To prevent the path issues we launch it explicitly with the full path
    python_wrapper_path = shutil.which("62-bin-python")
    if not copy_qr_code_path:
        print("[!] Could not find 'copy-qr-code' in PATH")
        exit(1)
    if not python_wrapper_path:
        print("[!] Could not find '62-bin-python' in PATH")
        exit(1)

    full_json = {
        "description": "+05 | #COPY-QR-CODE | AltGr+Q -> Take a screenshot and copy the contents of the QR code to clipboard",
        "manipulators": [
            {
                "from": {
                    "key_code": "q",
                    "modifiers": {
                        "mandatory": [
                            "right_option"
                        ],
                        "optional": [
                            "caps_lock"
                        ]
                    }
                },
                "to": [
                    {
                        "shell_command": f"{shlex.quote(python_wrapper_path)} {shlex.quote(copy_qr_code_path)} -d"
                    }
                ],
                "type": "basic"
            }
        ]
    }
    return full_json


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    # Insert the rule or print it depending on whether --print is supplied
    helper_default_argument_parser_action_handler(ap, generate_config_json)

