#!/usr/bin/env python3
import argparse
# local
from karabiner_config_helpers import helper_default_argument_parser_action_handler
from karabiner_config_helpers.type_utils import create_manipulators_for_all_layouts
from karabiner_config_helpers.app_layouts import load_app_resolver_map_json

DEFAULT_FROM_MODIFIER = "right_command"

def generate_config_json(args) -> dict:
    from_modifier = args.from_modifier
    sleep_seconds = 0

    app_resolver_map = load_app_resolver_map_json()
    manipulators: list[dict] = []

    def add_umlaut(umlaut_str: str, from_key: str) -> list[dict]:
        lower_case_manipulators = create_manipulators_for_all_layouts(umlaut_str.lower(), 0, from_key, [from_modifier], app_resolver_map, sleep_seconds)
        upper_case_manipulators = create_manipulators_for_all_layouts(umlaut_str.upper(), 0, from_key, [from_modifier, "shift"], app_resolver_map, sleep_seconds)
        return lower_case_manipulators + upper_case_manipulators

    manipulators += add_umlaut("ä", "a")
    manipulators += add_umlaut("ö", "o")
    manipulators += add_umlaut("ü", "u")
    # Nobody uses capital ß and python's .upper() method converts it to "SS" -> handle it manually
    manipulators += create_manipulators_for_all_layouts("ß", 0, "s", [from_modifier], app_resolver_map, sleep_seconds)

    return {
        "description": f"-45 | #UMLAUTS | {from_modifier}+a/o/u/s -> ä/ö/ü/ß (+shift for upper case)",
        "manipulators": manipulators,
    }


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-m", "--from-modifier", default=DEFAULT_FROM_MODIFIER, help=f"the modifier to use with the letter to type an umlaut. Default: {DEFAULT_FROM_MODIFIER}")

    helper_default_argument_parser_action_handler(ap, generate_config_json)
