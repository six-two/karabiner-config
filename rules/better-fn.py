#!/usr/bin/env python3
# Remaps fn to the OS's default modifier and caps lock to fn
import argparse
# local shared module
from karabiner_config_helpers import helper_default_argument_parser_action_handler
from karabiner_config_helpers.app_layouts import load_app_resolver_map_json


def get_capslock_manipulator() -> dict:
    return {
        "type": "basic",
        "from": {
            "key_code": "caps_lock",
            "modifiers": {
                "optional": [
                    "any"
                ]
            }
        },
        "to": [
            {
                "key_code": "fn",
                "lazy": True
            }
        ],
        "to_if_alone": [
            {
                "shell_command": "/Applications/flameshot.app/Contents/MacOS/flameshot gui"
            }
        ]
    }


def generate_config_json(args) -> dict:
    app_resolver_map = load_app_resolver_map_json()
    manipulators = [get_capslock_manipulator()]
    for default_modifier, condition_list in app_resolver_map.get_default_modifier_keys_and_conditions():
        # it just gave us an abstract value like "command", but we need to convert it to a real key like "left_command"
        default_modifier_key = f"left_{default_modifier}"
        manipulators.append({
            "type": "basic",
            "from": {
                "key_code": "fn",
                "modifiers": {
                    "optional": [
                        "any",
                    ],
                },
            },
            "to": [
                {
                    "set_variable": {
                        "name": "fn_pressed",
                        "value": 1
                    },
                },
                {
                    "key_code": default_modifier_key,
                },
            ],
             "to_after_key_up": [
                {
                    "set_variable": {
                        "name": "fn_pressed",
                        "value": 0
                    }
                }
            ],
            "conditions": condition_list,
        })

    return {
        "description": "+80 | #MOD-FN | Better fn key: fn -> OS_DEFAULT_MODIFIER; caps -> fn (held) / screenshot (type)",
        "manipulators": manipulators,
    }


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    helper_default_argument_parser_action_handler(ap, generate_config_json)

