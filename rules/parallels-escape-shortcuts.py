#!/usr/bin/env python3
# Prefixes certain keyboard shortcuts with Parallel's keyboard escape sequence, so that they are interpreted by MacOS instead
import argparse
# local shared module
from karabiner_config_helpers import helper_default_argument_parser_action_handler


def remap_modifier_for_key(key: str, modifier_list: list[str]) -> dict:
    return {
        "type": "basic",
        "description": f"Pass {'+'.join(modifier_list+[key])} to host",
        "from": {
            "key_code": key,
            "modifiers": {
                "mandatory": modifier_list,
                "optional": [
                    "caps_lock",
                ],
            },
        },
        "to": [
            # This is somehow required to release the modifiers held down by the user
            {
                "key_code": "vk_none",
            },
            # Press Ctrl+Alt to escape keyboard grabbing of Parallels
            {
                "key_code": "vk_none",
                "modifiers": [
                    "left_control",
                    "left_alt",
                ],
            },
            # Wait a little (for Parallels to release the keyboard)
            {
                "key_code": "vk_none",
                "hold_down_milliseconds": 20,
            },
            # Type the original keyboard shortcut
            {
                "key_code": key,
                "modifiers": modifier_list,
            },
        ],
        "conditions": [
            # Only apply this rule when Parallels is in focus
            {
                "type": "frontmost_application_if",
                "bundle_identifiers": [
                    "^com\\.parallels\\.desktop\\.console$",
                ],
            },
        ]
    }


def all_directions(*modifiers) -> list[str]:
    return ["+".join([*modifiers, direction_key]) for direction_key in ["left_arrow", "right_arrow", "up_arrow", "down_arrow"]]


def generate_config_json(args) -> dict:
    shortcuts: list[str] = []
    # Application switching
    # @TODO: this does not work that well (for repeated presses), find a workaround
    shortcuts += ["command+tab", "command+shift+tab"]
    # Workspace switching
    shortcuts += ["control+left_arrow", "control+right_arrow"]
    for workspace_id in range(10): # workspaces: 0-9
        # @TODO: find other modifier? Ctrl+1 and so on may be used in Windows and Linux VM
        shortcuts.append(f"option+{workspace_id}") # switch to workspace
        shortcuts.append(f"option+shift+{workspace_id}") # switch to workspace

    for type_key_slot in ["1", "2", "3", "4", "5", "f2", "f3", "f4", "f5"]:
        # alt+F1 = Accessibility shortcut
        shortcuts.append(f"right_option+{type_key_slot}")

    # Window resizing
    shortcuts += all_directions("control", "command")
    shortcuts += all_directions("option", "command")

    manipulators = []
    for key_string in shortcuts:
        parts = key_string.split("+")
        modifiers = parts[:-1]
        key = parts[-1]
        manipulators.append(remap_modifier_for_key(key, modifiers))

    config_json = {
        "description": "+20 | #PARALLELS-ESCAPE-COMMON | Prefixes common keyboard shortcuts with a parallels escape if the current application is Parallels",
        "manipulators": manipulators,
    }

    return config_json


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    helper_default_argument_parser_action_handler(ap, generate_config_json)

