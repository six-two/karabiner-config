#!/usr/bin/env python3
import argparse
import json
import os
import platform
import re
import shlex
import shutil
import subprocess
import sys
import time

SCRIPT_PATH = os.path.realpath(__file__)

### Start: Taken from https://gitlab.com/six-two/bin/-/blob/main/general/copy-qr-code?ref_type=heads

class MissingProgramException(Exception):
    pass


def generate_copy_command():
    os_type = platform.system()
    if os_type == "Linux":
        if os.getenv("XDG_SESSION_TYPE") == "wayland":
            if shutil.which("wl-copy"):
                return ["wl-copy"]
            else:
                raise MissingProgramException("No linux wayland copy tool found. Supported: wl-copy from wl-clipboard")
        else:
            if shutil.which("xclip"):
                return ["xclip", "-in", "-selection", "clipboard"]
            else:
                raise MissingProgramException("No linux X11 copy tool found. Supported: xclip")
    elif os_type == "Darwin":
        if shutil.which("pbcopy"):
            # This seems to be installed by default on MacOS and xclip did not work because of an error: "Error: Can't open display: (null)""
            return ["pbcopy"]
        else:
            raise MissingProgramException("No MacOS copy tool found. Supported: pbcopy")
    else:
        raise MissingProgramException(f"Unknown system platform '{os_type}', expected 'Linux' or 'Darwin' (MacOS)")

### End: https://gitlab.com/six-two/bin/-/blob/main/general/copy-qr-code?ref_type=heads

def load_counter(state_file: str) -> int:
    """
    Return the state stored in the file. Graceful error handling defaults to returning 0 th try to self-heal the application
    """
    try:
        with open(state_file) as f:
            state_str = f.read()
        return int(state_str)
    except ValueError as ex:
        print(f"[-] Failed to parse state '{state_str}', expected an integer:", ex)
        return 0
    except Exception as ex:
        print(f"[-] Failed to read state file {state_file}: ", ex)
        return 0


def store_counter(state_file: str, value: int) -> None:
    try:
        os.makedirs(os.path.dirname(state_file), exist_ok=True)
        with open(state_file, "w") as f:
            f.write(str(value))
    except Exception as ex:
        print(f"[-] Failed to write state file {state_file}: ", ex)
        return 0


def copy_to_clipboard(text: str) -> None:
    try:
        command = generate_copy_command()
        result = subprocess.run(command, input=text, encoding="utf-8")
        if result.returncode != 0:
            print(f"[!] Failed to copy to clipboard: {' '.join(command)} returned with code {result.returncode}")
            exit(1)
        else:
            print(f"[*] Copied: {text}")
    except MissingProgramException as ex:
        print("[!] Failed to find required external program")
        print(ex)
        exit(1)


def main_run(args):
    state_file = str(args.state)
    payload = str(args.payload)
    
    counter = load_counter(state_file)
    payload = payload.replace("$COUNTER", str(counter))
    
    copy_to_clipboard(payload)

    # Only store it if the copying worked
    store_counter(state_file, counter + 1)


def main():
    # @TODO: needs to be able to run without args to work as a proper rule

    state_default = os.path.expanduser("~/.config/xss-payload-counter")
    default_sleep = 200
    # Payload will leave behind a request in Burpsoute that is easy to find and for logging (x_123), display the number (123) and cause a popup with it, so that it is easy to see which insertion point is vulnerable
    default_payload = "$COUNTER<img onerror=confirm($COUNTER) src=x_$COUNTER}>"
    default_description = "Create unique XSS payload and insert it via clipboard"
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--state", default=state_default, help=f"the file containing the counter state (default: {state_default}")
    sp = ap.add_subparsers(dest="cmd")

    run_ap = sp.add_parser("run")
    # run_ap.add_argument("--sleep", default=default_sleep, type=int, help=f"how many milliseconds to wait after copying the payload to the parser (default: {default_sleep})")
    run_ap.add_argument("-p", "--payload", default=default_payload, help=f"payload to insert. Any occurrences of '$COUNTER' will be replaced with the current state (default: {default_payload})")
    
    karabiner_ap = sp.add_parser("karabiner")
    karabiner_ap.add_argument("keybinding_key", help="what key do you want to press to trigger the typing (example: t)")
    karabiner_ap.add_argument("keybinding_modifiers", nargs="*", help="what modifiers need to be pressed for the keybinding (example: 'right_command' 'right_meta')")
    karabiner_ap.add_argument("--sleep", default=default_sleep, type=int, help=f"how many milliseconds to wait after copying the payload to the parser (default: {default_sleep})")
    karabiner_ap.add_argument("-o", "--options", default="", help="a string containing all the options to call the run command, when it is invoked (default: '')")
    #@TODO: load them from a config
    karabiner_ap.add_argument("--non-mac-applications", nargs="*", default=[], help="applications that should be receiving Ctrl+V instead of Cmd+V for pasting. Useful for RDP, VMs, etc. Expected format is the application's bundle identifier like 'com.parallels.desktop.console', which can be found with Karabiner's EventViewer")
    karabiner_ap.add_argument("-d", "--description", default=default_description, help=f"the description that the Karabiner rule should have (default: '{default_description}')")

    args = ap.parse_args()

    if not args.cmd:
        print("[!] You need to specify a subcommand")
        ap.print_help()
        exit(1)
    elif args.cmd == "run":
        main_run(args)
    elif args.cmd == "karabiner":
        main_karabiner(args)
    else:
        print(f"[!] Unknown subcommand: {args.cmd}")

###############################################################################################
        
def main_karabiner(args):
    key = args.keybinding_key
    modifiers = args.keybinding_modifiers
    sleep_millis = args.sleep
    run_command_options = args.options
    non_mac_os_application_regex_list = [f"^{re.escape(bundle_id)}$" for bundle_id in args.non_mac_applications]
    description = args.description

    mac_os_paste_keys = {
        "key_code": "v",
        "modifiers": [
            "left_command"
        ]
    }

    if non_mac_os_application_regex_list:
        linux_or_windows_paste_keys = {
            "key_code": "v",
            "modifiers": [
                "left_control"
            ]
        }

        mac_conditions = {
            "type": "frontmost_application_unless",
            "bundle_identifiers": non_mac_os_application_regex_list
        }

        linux_or_windows_conditions = {
            "type": "frontmost_application_if",
            "bundle_identifiers": non_mac_os_application_regex_list
        }

        manipulators = [
            create_karabiner_manipulator(key, modifiers, run_command_options, sleep_millis, mac_os_paste_keys, mac_conditions),
            create_karabiner_manipulator(key, modifiers, run_command_options, linux_or_windows_paste_keys, linux_or_windows_conditions),
        ]
    else:
        manipulators = [create_karabiner_manipulator(key, modifiers, sleep_millis, run_command_options, mac_os_paste_keys, [])]

    config = {
        "description": description,
        "manipulators": manipulators
    }

    json.dump(config, sys.stdout, indent=4)


def create_karabiner_manipulator(key: str, modifiers: list[str], sleep_millis: int, run_command_options: str, paste_keys: dict, conditions: list[dict]):
    return {
        "type": "basic",
        "from": {
            "key_code": key,
            "modifiers": {
                "mandatory": modifiers,
                "optional": [
                    "caps_lock"
                ]
            }
        },
        "to": [
            {
                "shell_command": f"{shlex.quote(SCRIPT_PATH)} run {run_command_options}"
            }
        ],
        "to_delayed_action": {
            "to_if_invoked": [paste_keys],
            "to_if_canceled": [paste_keys]
        },
        "parameters": {
            "basic.to_delayed_action_delay_milliseconds": sleep_millis
        },
        "conditions": conditions
    }

if __name__ == "__main__":
    main()
