#!/usr/bin/env python3
import argparse
import os
import shlex
# local shared package
from karabiner_config_helpers import helper_default_argument_parser_action_handler

DEFAULT_ID = "CB_UPDATE_MULTIPLE"
DEFAULT_KEYS = ["1", "2", "3", "4", "5", "f2", "f3", "f4", "f5"]
DEFAULT_FROM_MODIFIER = "right_option"
DEFAULT_HOLD_MILLIS = 250


def generate_config_json(args) -> dict:
    to_clipboard_hold_millis = args.hold_millis
    rule_id = args.id
    modifier = args.from_modifier
    key_list = args.keys

    SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

    # Make sure that this script is in the correct location
    script_path = os.path.join(SCRIPT_DIR, "type-clipboard-update-script.sh")
    if not os.path.exists(script_path):
        print("[!] File does not exist:", script_path)
        exit(1)

    manipulators = []

    for key in key_list:
        clipboard_to_karabiner_action = {
            "shell_command": f"{shlex.quote(script_path)} '-2 | #CB_TYPE_{key} | right_option+{key}: Type stored clipboard for {key}' --sleep {args.sleep} {modifier}+{key}"
        }

        manipulators.append({
            "type": "basic",
            "from": {
                "key_code": key,
                "modifiers": {
                    "mandatory": [
                        modifier,
                        "shift",
                    ],
                    "optional": [
                        "caps_lock",
                    ],
                },
            },
            "parameters": {
                "basic.to_if_alone_timeout_milliseconds": to_clipboard_hold_millis,
                "basic.to_if_held_down_threshold_milliseconds": to_clipboard_hold_millis
            },
            "to_if_alone": [
                # Short type: write current clipboard to config
                clipboard_to_karabiner_action
            ],
            "to_if_held_down": [
                # Hold button down
                # Copy current selection by sending Command-C write the new clipboard to config
                #@TODO check correct keyboard combi for OS
                {
                    "key_code": "c",
                    "modifiers": [
                        "left_command",
                    ],
                },
                # Wait a little
                {
                    "key_code": "vk_none",
                    "hold_down_milliseconds": 150,
                },
                # Write current clipboard to config
                clipboard_to_karabiner_action,
            ],
        })

    full_json = {
        "description": f"-1 | #{rule_id} | {modifier}+{'/'.join(key_list)} -> With shift: Store clipboard contents / Without shift: Type stored contents",
        "manipulators": manipulators,
    }
    return full_json


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--id", default=DEFAULT_ID, help=f"the rule ID to use. Rules with the same ID will be replaced. Default: {DEFAULT_ID}")
    ap.add_argument("--keys", default=DEFAULT_KEYS, nargs="+", help=f"the keys that copy slots should be created for. Default: {', '.join(DEFAULT_KEYS)}")
    ap.add_argument("-m", "--from-modifier", default=DEFAULT_FROM_MODIFIER, help=f"the modifier key to use for triggering the shortcut. May not contain shift, because it is needed for the read from clipboard and type clipboard differentiation. Default: {DEFAULT_FROM_MODIFIER}")
    ap.add_argument("-d", "--hold-millis", type=int, default=DEFAULT_HOLD_MILLIS, help=f"after how many milli seconds of wolding down the mouse button to first copy the currently selected text. Default: {DEFAULT_HOLD_MILLIS}")
    ap.add_argument("-s", "--sleep", type=float, default=1, help="how long to wait before starting the typing")

    # Insert the rule or print it depending on whether --print is supplied
    helper_default_argument_parser_action_handler(ap, generate_config_json)
