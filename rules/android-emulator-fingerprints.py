#!/usr/bin/env python3
import argparse
# local
from karabiner_config_helpers import helper_default_argument_parser_action_handler

DEFAULT_FROM_MODIFIER = "right_command"

def generate_config_json(args) -> dict:
    manipulators: list[dict] = []

    for fingerprint_id in range(1,10):
        fingerprint_keybinding = {
            "type": "basic",
            "from": {
                "key_code": f"f{fingerprint_id}",
                "modifiers": {
                    "mandatory": [
                    ],
                    "optional": [
                        "caps_lock"
                    ]
                }
            },
            "to": [
                {
                    "shell_command": f"~/Library/Android/sdk/platform-tools/adb -e emu finger touch {fingerprint_id}"
                }
            ],
            "conditions": [
                {
                    "type": "frontmost_application_if",
                    # For the android studio emulator (~/Library/Android/sdk/emulator/qemu/darwin-aarch64/qemu-system-aarch64) the bundleID is empty.
                    # So we can hopefully match only this application
                    # This allows us to not require any modifiers for the key binding
                    "bundle_identifiers": [
                        "^$"
                    ]
                }
            ]
        }
        manipulators.append(fingerprint_keybinding)

    return {
        "description": f"-20 | #ANDROID-EMULATOR-FINGERPRINTS | Android Emulator: F1, F2, ..., F9 -> adb -e emu finger touch <X>",
        "manipulators": manipulators,
    }


if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Generate key bindings in Android Emulator to do fingerprint auth using the F1, F2, etc keys for each fingerprint")

    helper_default_argument_parser_action_handler(ap, generate_config_json)
