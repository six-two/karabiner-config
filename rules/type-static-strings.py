#!/usr/bin/env python3
# Type static strings defined in a user supplied JSON file.
# This can be used to auto-type usernames, email-addresses, common text snippets, etc
import argparse
import os
import subprocess
import sys


def debug(message: str):
    print(message, file=sys.stderr)


def delegate_to_subprocess(args) -> dict:
    custom_strings_json_file = os.path.expanduser("~/.config/karabiner/type-static-strings.json")
    if not os.path.exists(custom_strings_json_file):
        SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
        strings_json_file = os.path.realpath(os.path.join(SCRIPT_DIR, "..", "type-static-strings.json"))
        if not os.path.exists(strings_json_file):
            debug(f"[!] {strings_json_file} does not exist")
            exit(1)
        else:
            debug(f"[*] Using default file. Copy {strings_json_file} to {custom_strings_json_file} and edit it to type your own strings.")
    else:
        debug(f"[*] Using strings to type from {custom_strings_json_file}")
        strings_json_file = custom_strings_json_file

    description = "+10 | #TYPE-STATIC-STRINGS | Type strings from type-static-strings.json"
    command = ["karabiner-type-multiple", "--file", strings_json_file, "--description", description]
    if args.print:
        command.append("--print")
    subprocess.check_call(command)


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--print", action="store_true", help="instead of inserting the rule just print it to stdout")
    args = ap.parse_args()

    delegate_to_subprocess(args)

    debug("[*] If you want to autotype passwords that should not be stored easily reversible on the disk (the Karabiner config can always be reversed), copy them from your password manager and run the following command:")
    debug("pbpaste | karabiner-type-multiple --stdin --description '+15 | #TYPE-STATIC-STRINGS-2 | Type strings from clipboard config'")
