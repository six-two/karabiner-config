#!/usr/bin/env bash

if [[ $# -lt 2 || -z "$1" || -z "$2" ]]; then
    echo "[!] Usage: <DESCRIPTION> <KEY_BINDING> [...MORE_KEYBINDINGS]"
    echo '    Example usage: "-2 | #CB_TYPE_V | right_option+v / button4: Type clipboard" right_option+v button4'
    exit 1
fi

DESCRIPTION="$1"
shift

# cd into the repo's root dir
cd "$( dirname "${BASH_SOURCE[0]}" )/.."

####################### venv #######################
if [[ ! -f "venv/bin/activate" ]]; then
    echo "[*] Creating virtual python environment"
    python3 -m venv --clear --upgrade-deps "venv"
fi

echo "[*] Using virtual python environment"
source "venv/bin/activate"

python3 -m pip install -U "./karabiner-config-helpers/"

####################### main action #######################

# read the clipboard, convert it to a complex modification and then insert/deplace it into Karabiner's config file
pbpaste | karabiner-type "$@" --stdin -d "$DESCRIPTION"
