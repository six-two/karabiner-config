#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# cd into the repo's root dir
cd "$( dirname "${BASH_SOURCE[0]}" )"

####################### venv #######################
if [[ ! -f "$SCRIPT_DIR/venv/bin/activate" ]]; then
    echo "[*] Creating virtual python environment"
    python3 -m venv --clear --upgrade-deps "$SCRIPT_DIR/venv"
fi

echo "[*] Using virtual python environment"
source "$SCRIPT_DIR/venv/bin/activate"

python3 -m pip install -U "$SCRIPT_DIR/karabiner-config-helpers/"

####################### delegate to venv's python #######################

python3 "$@"
