# karabiner-config-helpers

This contains some common code for my personal [Karabiner Elements](https://karabiner-elements.pqrs.org/) rules, like some keyboard mappings (macOS US, Linux US de_se_fi), code to parse and modify rules in your `~/.config/karabiner/karabiner.json` and helper methods for handling apps with different keyboard layouts (VMs, RDP, Citrix, etc).

This package is a subset of <https://gitlab.com/six-two/karabiner-config>.
There you can also find my rules in the `rules/` directory.
