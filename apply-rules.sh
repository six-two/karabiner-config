#!/usr/bin/env bash

# cd into the repo's root dir
cd "$( dirname "${BASH_SOURCE[0]}" )"

####################### venv #######################
if [[ ! -f venv/bin/activate ]]; then
    echo "[*] Creating virtual python environment"
    python3 -m venv --clear --upgrade-deps venv
fi

echo "[*] Using virtual python environment"
source venv/bin/activate

python3 -m pip install ./karabiner-config-helpers/

####################### process rules #######################

for FILE in rules/*; do
    if [[ "$FILE" == *.json ]]; then
        echo "[*] Processing JSON: $FILE"
        karabiner-rules insert "$FILE"
    elif [[ "$FILE" == *.py ]]; then
        echo "[*] Processing Pyhton: $FILE"
        python3 "$FILE"
    else
        echo "[-] Unsupported file type: $FILE"
    fi
done
