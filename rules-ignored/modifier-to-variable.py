#!/usr/bin/env python3
import argparse
# local shared module
from karabiner_config_helpers import helper_default_argument_parser_action_handler
from karabiner_config_helpers.app_layouts import load_app_resolver_map_json

DEFAULT_TYPE = ["right_option"]
DEFAULT_NO_TYPE = ["right_command"]

def get_key_to_variable_manipulator(modifier_name: str, still_type_original_key: bool) -> dict:
    variable_name = f"{modifier_name}_pressed"
    original_key_action = []
    if still_type_original_key:
        original_key_action.append({
            "key_code": modifier_name,
        })
    return {
        "type": "basic",
        "from": {
            "key_code": modifier_name,
            "modifiers": {
                "optional": [
                    "any",
                ],
            },
        },
        "to": [
            {
                "set_variable": {
                    "name": variable_name,
                    "value": 1,
                },
            },
            *original_key_action,
        ],
        "to_after_key_up": [
            {
                "set_variable": {
                    "name": variable_name,
                    "value": 0
                },
            },
        ],
    }


def generate_config_json(args) -> dict:
    manipulators = [get_key_to_variable_manipulator(modifier, True) for modifier in args.type]
    manipulators += [get_key_to_variable_manipulator(modifier, False) for modifier in args.no_type]

    return {
        "description": f"+80 | #MOD-VARS | Store modifier state in <name>_pressed. Type: {args.type}. No type: {args.no_type}",
        "manipulators": manipulators,
    }


if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Map a key (modifier) to a variable and optionally suppress the original key press. For example right_command can be mapped to right_command_pressed")
    ap.add_argument("-t", "--type", nargs="*", default=DEFAULT_TYPE, help=f"keys that should still be pressed. Default: {DEFAULT_TYPE}")
    ap.add_argument("-T", "--no-type", nargs="*", default=DEFAULT_NO_TYPE, help=f"keys that should not be pressed. Default: {DEFAULT_NO_TYPE}")

    helper_default_argument_parser_action_handler(ap, generate_config_json)

