#!/usr/bin/env python3
# Issue somethines I type text like "I am an architect". The Spacebar+A sometimes overlap and then it removes everything (Cmd+A=Select everything, next character replaces text). This is too annoying, so this rule is disabled

# Remaps space to the OS's default modifier while pressed
# This is useful, since some/many external keyboards do not send fn key presses directly to the OS (instead they handle it internally)
import argparse
# local shared module
from karabiner_config_helpers import helper_default_argument_parser_action_handler
from karabiner_config_helpers.app_layouts import load_app_resolver_map_json


def generate_config_json(args) -> dict:
    app_resolver_map = load_app_resolver_map_json()
    manipulators = []
    for default_modifier, condition_list in app_resolver_map.get_default_modifier_keys_and_conditions():
        # it just gave us an abstract value like "command", but we need to convert it to a real key like "left_command"
        default_modifier_key = f"left_{default_modifier}"
        manipulators.append({
            "type": "basic",
            "from": {
                "key_code": "spacebar",
                "modifiers": {
                    "optional": [
                        "any",
                    ],
                },
            },
            "to": [
                {
                    "set_variable": {
                        "name": "spacebar_pressed",
                        "value": 1,
                    },
                },
                {
                    "key_code": default_modifier_key,
                    "lazy": True,
                },
            ],
             "to_after_key_up": [
                {
                    "set_variable": {
                        "name": "spacebar_pressed",
                        "value": 0
                    },
                }
            ],
            "to_if_alone": [
                {
                    "key_code": "spacebar",
                }
            ],
            "conditions": condition_list,
        })

    return {
        "description": "+80 | #MOD-SPACE | Better space key: spacebar -> OS_DEFAULT_MODIFIER when held",
        "manipulators": manipulators,
    }


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    helper_default_argument_parser_action_handler(ap, generate_config_json)

