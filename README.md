# Karabiner config helper

My tools and rules for keyboard remapping on macOS with [Karabiner-Elements](https://karabiner-elements.pqrs.org/).

This repo contains:

- `karabiner-config-helpers`: Common helper tools in a python package to prevent lots of duplicate code.
    You can also install it from PyPI with:
    ```bash
    pip install karabiner-config-helpers
    ```
- `rules`: My personal rules either as JSON files or python scripts
- `rules-ignored`: Some personal rules that are not applied in my current config.
    The rules might be old, no longer needed, work in progress or disabled for debugging purposes or workarounds.
- `apply-rules.sh`: This script installs `karabiner-config-helpers` in a local Python venv and then adds all rules in the `rules` directory in your Karabiner Elements configuration

## More configurations

This is a lift of other configurations (non-complex modifications) that I use in my setup.

1. Enable mouse events:
    `Karabiner Settings` -> `Devices` -> Section "Your mouse name" -> `Modify events`
2. In macOS settings use function keys by default (instead of media keys): Open MacOS setttings -> Sidebar entry `Keyboard` (near bottom) -> `Keyboard Shortcuts...` (bottom of second box) -> Sidebar entry `Function Keys` -> Enable `Use F1, F2, etc. keys as standard function keys`.
    See also [Apple documentation](https://support.apple.com/en-us/102439).
3. Disable Emoji Picker when pressing ++fn++: `Settings` -> `Keyboard` -> Set `Press [WorldSymbol] key to` to `Do Nothing`.

### Parallels VM

Getting keyboard shortcuts Parallels to work properly is hard.
Either it sends way to many key codes to the host (like Cmd+Space), or it does not send any.

You can choose in Parallels Doc Icon -> Settings (Golbal settings, **not** vm specific settings) -> Tab `Shortcuts` -> `macOS System Shortcuts` -> `Send macOS system shortcuts`.

The sanest way seems to be to go with the do not send any to host choice and then either manually release the mouse (cmd+alt), or to create special parallels rules that release the mouse cof common shortcuts like (Cmd+tab, ctrl+{1,4}, ctrl+{left,right}).



## Modify rules from the internet

When adding custom rules via the GUI and the custom rules are based on rules you downloaded, you need to remove the outer part:
```bash
{
  "title": "German Umlaut",
  "rules": [
    [...]
  ]
}
```

If you do not do it you get an error like ``error: `manipulators` is missing or empty in {"rules":[...``.

You can obtain the keycode names by going to simple modifications and selecting the from side.
The shown names in the drop downs should be all the ones usable in your configs.

Alternatively you can use the Karabiner-EventViewer application (installed along with Karabiner-Events) to show all events that carabiner sees.

!!! warn "Order"
    For me the EventViewer shows the key codes after remapping, so it can be used to check remappings.
    However, if you swapped keys, you will see the remapped key, not the original key code.

